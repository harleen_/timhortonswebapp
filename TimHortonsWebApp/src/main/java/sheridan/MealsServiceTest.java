package sheridan;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testDrinksRegular() {
		List<String> mealType = new ArrayList<String>( );
		mealType = MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("Invalid value for available meal type", mealType != null);
	}
	
	@Test
	public void testDrinksException() {
		List<String> mealType = new ArrayList<String>( );
		mealType = MealsService.getAvailableMealTypes(null);
		assertTrue("Invalid value for available meal type", mealType.get(0).equals("No Brand Available"));
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		List<String> mealType = new ArrayList<String>( );
		mealType = MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("Invalid value for available meal type", mealType.size() > 3);
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		List<String> mealType = new ArrayList<String>( );
		mealType = MealsService.getAvailableMealTypes(null);
		assertTrue("Invalid value for available meal type", mealType.size() == 1);
	}

}
